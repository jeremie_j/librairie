# Api Librairie

API for stocking books by ISBN code. 

Create book by entering an ISBN code of a book, and a new book will be created in the database with additionals info from the google books api:
- title
- authors
- published_year
- description
- categories
- thumbnail_url

You can start all the code with docker, it will create a app with a Database and Api services

Poetry is needed to manages packages locally in the project
```
pip install poetry
```
Start the database and api with:
```
// In the '/librairie' directory

docker compose build
docker compose up
```

### Access to database:
- host: `localhost:5432`
- user: `adminuser`
- password: `123455678`
- database: `database`

### Access to api:
- `localhost:8000`
- `localhost:8000/docs` for OpenApi documentation with all endpoints

### Fill the database:
Use the script `/librairie/backend/api/scripts/migrate_books.py` to fill the database with ISBN code from `fichierSource.csv`
```
// In '/librairie/backend/api'

poetry shell
poetry install 
export DATABASEURL=postgresql://adminuser:123455678@localhost:5432/database
python -m scripts.migrate_books.py
```
Running the script for 2-3 min is enough to have a large dataset
