import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import JSONB
from session import metadata

book = sa.Table(
    "book",
    metadata,
    sa.Column("isbn", sa.String, nullable=False, primary_key=True),
    sa.Column("data", JSONB, nullable=False),
    sa.Column("created_at", sa.DateTime, nullable=False),
)
