from pydantic import BaseModel
from typing import List, Optional


class BookStructure(BaseModel):
    title: str
    authors: Optional[List] = []
    publisher: Optional[str]
    published_year: Optional[str]
    description: Optional[str]
    categories: Optional[List] = []
    thumbnail_url: Optional[str]
