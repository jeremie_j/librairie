from fastapi import FastAPI
from schemas.book import BookStructure
from functions import book_functions
from session import engine, metadata

metadata.create_all(engine)
app = FastAPI()


@app.post("/books/{isbn}")
def new_book(isbn: str):
    return book_functions.create_book(isbn)


@app.get("/books/{isbn}")
def get_book(isbn: str):
    return book_functions.get_by_id(isbn)


@app.delete("/books/{isbn}")
def delete_book(isbn: str):
    return book_functions.delete_by_id(isbn)


@app.put("/books/{isbn}")
def update_book(isbn: str, book: BookStructure):
    return book_functions.update_by_id(isbn, book)


@app.get("/books/")
def all_book():
    return list(book_functions.get_all())
