import os
from sqlalchemy import create_engine, MetaData

engine = create_engine(os.getenv("DATABASEURL"))

metadata = MetaData()
