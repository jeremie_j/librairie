import isbnlib


def check_isbn(isbn: str):
    return isbnlib.is_isbn10(isbn) or isbnlib.is_isbn13(isbn)
