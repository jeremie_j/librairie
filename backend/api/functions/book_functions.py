import requests
from datetime import datetime

from fastapi.exceptions import HTTPException
from schemas.book import BookStructure
from models.book import book
from functions.utils import check_isbn


import sqlalchemy
from session import engine


def create_book(isbn: str):
    if not check_isbn(isbn):
        raise HTTPException(403, "Invalid isbn code")

    with engine.connect() as conn:
        if (
            conn.execute(sqlalchemy.select([book]).where(
                book.c.isbn == isbn)).first()
            is not None
        ):
            raise HTTPException(409, "This book is already in the database")

    response = requests.get(
        "https://www.googleapis.com/books/v1/volumes", params={"q": "isbn:" + isbn}
    )

    search_results = response.json().get("items")
    if search_results is None:
        raise HTTPException(404, f"No book found for isbn code: {isbn}")

    searched_book = search_results[0]["volumeInfo"]

    # Format date to YYYY
    published_year = searched_book.get("publishedDate")
    if published_year is not None:
        published_year = published_year.split("-")[0]

    new_book = BookStructure(
        title=searched_book.get("title"),
        authors=searched_book.get("authors"),
        publisher=searched_book.get("publisher"),
        published_year=published_year,
        description=searched_book.get("description"),
        categories=searched_book.get("categories"),
        thumbnail_url=searched_book.get("imageLinks", {}).get("thumbnail"),
    )

    with engine.connect() as conn:
        stmt = sqlalchemy.insert(
            book,
            {"isbn": isbn, "data": new_book.dict(), "created_at": datetime.now()},
        )
        conn.execute(stmt)

    return {isbn: isbn, **new_book.dict()}


def get_by_id(isbn: str):
    if not check_isbn(isbn):
        raise HTTPException(403, "Invalid isbn code")
    with engine.connect() as conn:
        result = conn.execute(sqlalchemy.select(
            [book]).where(book.c.isbn == isbn))
        row = result.first()

        if row is None:
            raise HTTPException(404, "Isbn not found")

        return {"isbn": row.isbn, **row.data}


def delete_by_id(isbn: str):
    if not check_isbn(isbn):
        raise HTTPException(403, "Invalid isbn code")
    with engine.connect() as conn:
        return conn.execute(
            sqlalchemy.delete(book).where(book.c.isbn == isbn).returning(book)
        ).first()


def update_by_id(isbn: str, updated_book: BookStructure):
    if not check_isbn(isbn):
        raise HTTPException(403, "Invalid isbn code")
    with engine.connect() as conn:
        return conn.execute(
            sqlalchemy.update(book)
            .where(book.c.isbn == isbn)
            .values(data=updated_book.dict())
            .returning(book)
        ).first()


def get_all():
    with engine.connect() as conn:
        result = conn.execute(sqlalchemy.select([book]))
        for row in result:
            yield {"isbn": row.isbn, **row.data}
