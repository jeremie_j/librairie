import sqlalchemy
import requests
from schemas.book import BookStructure
from session import engine
from functions.utils import check_isbn
from models.book import book
from datetime import datetime

with open("./scripts/data/fichierSource.csv") as file:
    for ligne in file:
        isbn = ligne.rstrip()
        if not check_isbn(isbn):
            print("Invalid isbn")
            continue

        with engine.connect() as conn:
            if (
                conn.execute(
                    sqlalchemy.select([book]).where(book.c.isbn == isbn)
                ).first()
                is not None
            ):
                print(f"Duplicate book with isbn {isbn}")
                continue

        for i in range(0, 5):
            response = requests.get(
                "https://www.googleapis.com/books/v1/volumes",
                params={"q": "isbn:" + isbn},
            )
            if response.status_code == 429:
                # Too much request for google books api, retrying
                continue
            elif response.status_code == 200:
                break
        search_results = response.json().get("items")
        if search_results is None:
            continue
        elif search_results is None:
            print(f"No book found or too much request for google book api")
            continue

        searched_book = search_results[0]["volumeInfo"]

        published_year = searched_book.get("publishedDate")
        if published_year is not None:
            published_year = published_year.split("-")[0]

        new_book = BookStructure(
            title=searched_book.get("title"),
            authors=searched_book.get("authors"),
            publisher=searched_book.get("publisher"),
            published_year=published_year,
            description=searched_book.get("description"),
            categories=searched_book.get("categories"),
            thumbnail_url=searched_book.get("imageLinks", {}).get("thumbnail"),
        )

        with engine.connect() as conn:
            stmt = sqlalchemy.insert(
                book,
                {"isbn": isbn, "data": new_book.dict(), "created_at": datetime.now()},
            )
            conn.execute(stmt)
            print(f"Book with isbn {isbn} created")
